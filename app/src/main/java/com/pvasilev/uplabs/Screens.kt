package com.pvasilev.uplabs

import com.pvasilev.uplabs.presentation.*
import com.pvasilev.uplabs.presentation.collections.CollectionsContainerFragment
import com.pvasilev.uplabs.presentation.collections.PopularCollectionsFragment
import com.pvasilev.uplabs.presentation.collections.SavedCollectionsFragment
import com.pvasilev.uplabs.presentation.followers.FollowersFragment
import com.pvasilev.uplabs.presentation.followings.FollowingsFragment
import com.pvasilev.uplabs.presentation.leaders.LeadersFragment
import com.pvasilev.uplabs.presentation.post.PostFragment
import com.pvasilev.uplabs.presentation.posts.PostsFragment
import com.pvasilev.uplabs.presentation.profile.ProfileFragment
import ru.terrakok.cicerone.android.support.SupportAppScreen

object Screens {
    object MainFlowScreen : SupportAppScreen() {
        override fun getFragment() = MainFlowFragment()
    }

    object PostsFlowScreen : SupportAppScreen() {
        override fun getFragment() = PostsFlowFragment()
    }

    object CollectionsFlowScreen : SupportAppScreen() {
        override fun getFragment() = CollectionsFlowFragment()
    }

    object LeadersFlowScreen : SupportAppScreen() {
        override fun getFragment() = LeadersFlowFragment()
    }

    class ProfileFlowScreen(private val nickname: String) : SupportAppScreen() {
        override fun getFragment() = ProfileFlowFragment.newInstance(nickname)
    }

    object PostsScreen : SupportAppScreen() {
        override fun getFragment() = PostsFragment()
    }

    object CollectionsContainerScreen : SupportAppScreen() {
        override fun getFragment() = CollectionsContainerFragment()
    }

    object LeadersScreen : SupportAppScreen() {
        override fun getFragment() = LeadersFragment()
    }

    object PopularCollectionsScreen : SupportAppScreen() {
        override fun getFragment() = PopularCollectionsFragment()
    }

    object SavedCollectionsScreen : SupportAppScreen() {
        override fun getFragment() = SavedCollectionsFragment()
    }

    data class PostScreen(private val postId: String) : SupportAppScreen() {
        override fun getFragment() = PostFragment.newInstance(postId)
    }

    data class ProfileScreen(private val nickname: String) : SupportAppScreen() {
        override fun getFragment() = ProfileFragment.newInstance(nickname)
    }

    data class FollowersScreen(private val nickname: String) : SupportAppScreen() {
        override fun getFragment() = FollowersFragment.newInstance(nickname)
    }

    data class FollowingsScreen(private val nickname: String) : SupportAppScreen() {
        override fun getFragment() = FollowingsFragment.newInstance(nickname)
    }
}