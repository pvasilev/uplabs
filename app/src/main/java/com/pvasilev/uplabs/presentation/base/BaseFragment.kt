package com.pvasilev.uplabs.presentation.base

import androidx.fragment.app.Fragment
import com.pvasilev.uplabs.presentation.OnBackPressedListener

abstract class BaseFragment<T : BaseViewModel> : Fragment(),
    OnBackPressedListener {

    protected val vm: T by lazy { provideViewModel() }

    abstract fun provideViewModel(): T

    override fun onBackPressed() {
        vm.onBackPressed()
    }
}