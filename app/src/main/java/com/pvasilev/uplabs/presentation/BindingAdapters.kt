package com.pvasilev.uplabs.presentation

import android.view.View
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.pvasilev.uplabs.data.model.PostTeaser

@BindingAdapter("app:imageUrl", "app:circular", requireAll = false)
fun loadImage(view: ImageView, url: String?, circular: Boolean) {
    if (url != null) {
        Glide.with(view)
            .asBitmap()
            .load(url)
            .apply(if (circular) RequestOptions().circleCrop() else RequestOptions())
            .into(view)
    }
}

@BindingAdapter("app:imageUrls")
fun loadImages(view: ImageView, teasers: List<PostTeaser>) {
    Glide.with(view)
        .load(teasers.map { it.teaserUrl })
        .apply(RequestOptions().diskCacheStrategy(DiskCacheStrategy.NONE))
        .into(view)
}

@BindingAdapter("app:items")
fun <T : Any> setItems(recyclerView: RecyclerView, items: List<T>?) {
    if (recyclerView.adapter is ListAdapter<*, *>) {
        (recyclerView.adapter as ListAdapter<T, *>).submitList(items)
    }
}

@BindingAdapter("app:goneUnless")
fun goneUnless(view: View, predicate: Boolean) {
    view.visibility = if (predicate) {
        View.VISIBLE
    } else {
        View.GONE
    }
}