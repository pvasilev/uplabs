package com.pvasilev.uplabs.presentation.profile

import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import com.pvasilev.uplabs.data.model.Post
import com.pvasilev.uplabs.data.model.User
import com.pvasilev.uplabs.domain.GetUserPostsUseCase
import com.pvasilev.uplabs.presentation.base.BaseViewModel
import com.pvasilev.uplabs.Screens
import kotlinx.coroutines.launch
import ru.terrakok.cicerone.Router
import javax.inject.Inject

class ProfileViewModel @Inject constructor(
    router: Router,
    private val nickname: String,
    private val getUserPosts: GetUserPostsUseCase
) : BaseViewModel(router) {

    val posts: MutableLiveData<List<Post>> = MutableLiveData()

    val user: MutableLiveData<User> = MediatorLiveData<User>().apply {
        addSource(posts) {
            val post = it.firstOrNull()
            if (post != null) {
                postValue(post.author)
            }
        }
    }

    init {
        launch {
            posts.postValue(getUserPosts(nickname))
        }
    }

    fun onFollowersClicked() {
        router.navigateTo(Screens.FollowersScreen(nickname))
    }

    fun onFollowingClicked() {
        router.navigateTo(Screens.FollowingsScreen(nickname))
    }

    fun onPostClicked(post: Post) {
        router.navigateTo(Screens.PostScreen(post.id))
    }
}