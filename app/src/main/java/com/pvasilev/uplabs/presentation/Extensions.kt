package com.pvasilev.uplabs.presentation

import com.bumptech.glide.Priority
import com.bumptech.glide.load.data.DataFetcher
import com.bumptech.glide.load.data.HttpUrlFetcher
import java.io.InputStream
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine

suspend fun HttpUrlFetcher.await() = suspendCoroutine<InputStream> { continuation ->
    val callback = object : DataFetcher.DataCallback<InputStream> {
        override fun onLoadFailed(e: Exception) {
            continuation.resumeWithException(e)
        }

        override fun onDataReady(data: InputStream?) {
            continuation.resume(data!!)
        }

    }
    loadData(Priority.HIGH, callback)
}