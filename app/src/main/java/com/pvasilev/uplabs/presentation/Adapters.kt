package com.pvasilev.uplabs.presentation

import androidx.recyclerview.widget.DiffUtil
import com.pvasilev.uplabs.R
import com.pvasilev.uplabs.data.model.Collection
import com.pvasilev.uplabs.data.model.Comment
import com.pvasilev.uplabs.data.model.Post
import com.pvasilev.uplabs.data.model.User
import com.pvasilev.uplabs.presentation.base.BaseAdapter
import com.pvasilev.uplabs.presentation.recyclerview.OnItemClickListener

class UsersAdapter(onItemClickListener: OnItemClickListener<User>) :
    BaseAdapter<User>(onItemClickListener, UsersAdapter) {
    override val layoutResId: Int
        get() = R.layout.item_user

    companion object : DiffUtil.ItemCallback<User>() {
        override fun areItemsTheSame(oldItem: User, newItem: User): Boolean = oldItem.id == newItem.id

        override fun areContentsTheSame(oldItem: User, newItem: User): Boolean = oldItem == newItem
    }
}

class LeadersAdapter(onItemClickListener: OnItemClickListener<User>) :
    BaseAdapter<User>(onItemClickListener, LeadersAdapter) {
    override val layoutResId: Int
        get() = R.layout.item_leader

    companion object : DiffUtil.ItemCallback<User>() {
        override fun areItemsTheSame(oldItem: User, newItem: User): Boolean = oldItem.id == newItem.id

        override fun areContentsTheSame(oldItem: User, newItem: User): Boolean = oldItem == newItem
    }
}

class GalleryAdapter(onItemClickListener: OnItemClickListener<Post>) :
    BaseAdapter<Post>(onItemClickListener, GalleryAdapter) {
    override val layoutResId: Int
        get() = R.layout.item_gallery

    companion object : DiffUtil.ItemCallback<Post>() {
        override fun areItemsTheSame(oldItem: Post, newItem: Post): Boolean = oldItem.id == newItem.id

        override fun areContentsTheSame(oldItem: Post, newItem: Post): Boolean = oldItem == newItem
    }
}

class RelatedAdapter(onItemClickListener: OnItemClickListener<Post>) :
    BaseAdapter<Post>(onItemClickListener, RelatedAdapter) {
    override val layoutResId: Int
        get() = R.layout.item_related

    companion object : DiffUtil.ItemCallback<Post>() {
        override fun areItemsTheSame(oldItem: Post, newItem: Post): Boolean = oldItem.id == newItem.id

        override fun areContentsTheSame(oldItem: Post, newItem: Post): Boolean = oldItem == newItem
    }
}

class PostsAdapter(onItemClickListener: OnItemClickListener<Post>) :
    BaseAdapter<Post>(onItemClickListener, PostsAdapter) {
    override val layoutResId: Int
        get() = R.layout.item_post

    companion object : DiffUtil.ItemCallback<Post>() {
        override fun areItemsTheSame(oldItem: Post, newItem: Post): Boolean = oldItem.id == newItem.id

        override fun areContentsTheSame(oldItem: Post, newItem: Post): Boolean = oldItem == newItem
    }
}

class CollectionsAdapter(onItemClickListener: OnItemClickListener<Collection>) :
    BaseAdapter<Collection>(onItemClickListener, CollectionsAdapter) {
    override val layoutResId: Int
        get() = R.layout.item_collection

    companion object : DiffUtil.ItemCallback<Collection>() {
        override fun areItemsTheSame(oldItem: Collection, newItem: Collection): Boolean = oldItem.id == newItem.id

        override fun areContentsTheSame(oldItem: Collection, newItem: Collection): Boolean = oldItem == newItem
    }
}

class CommentsAdapter : BaseAdapter<Comment>(diffCallback = CommentsAdapter) {
    override val layoutResId: Int
        get() = R.layout.item_comment

    companion object : DiffUtil.ItemCallback<Comment>() {
        override fun areItemsTheSame(oldItem: Comment, newItem: Comment): Boolean = oldItem.id == newItem.id

        override fun areContentsTheSame(oldItem: Comment, newItem: Comment): Boolean = oldItem == newItem
    }
}

typealias FollowersAdapter = UsersAdapter

typealias FollowingsAdapter = UsersAdapter