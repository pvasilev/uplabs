package com.pvasilev.uplabs.presentation.glide

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Canvas
import android.graphics.Paint
import com.bumptech.glide.Priority
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.data.DataFetcher
import com.bumptech.glide.load.data.HttpUrlFetcher
import com.bumptech.glide.load.model.GlideUrl
import com.pvasilev.uplabs.presentation.await
import kotlinx.coroutines.runBlocking
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import java.io.InputStream

class MultipleUrlDataFetcher(private val model: List<*>) : DataFetcher<InputStream> {
    override fun getDataClass() = InputStream::class.java

    override fun getDataSource() = DataSource.REMOTE

    override fun cleanup() {
    }

    override fun cancel() {
    }

    override fun loadData(priority: Priority?, callback: DataFetcher.DataCallback<in InputStream>) {
        runBlocking {
            val urls = model as List<String>
            val bitmaps = mutableListOf<Bitmap>()
            try {
                for (url in urls) {
                    val stream = HttpUrlFetcher(GlideUrl(url), 30000).await()
                    val bitmap = BitmapFactory.decodeStream(stream)
                    bitmaps += bitmap
                }
                val bitmap = when (bitmaps.size) {
                    1 -> mergeBitmaps(bitmaps, 1, 1)
                    2 -> mergeBitmaps(bitmaps, 1, 2)
                    3 -> mergeBitmaps(bitmaps, 1, 3)
                    4 -> mergeBitmaps(bitmaps, 2, 2)
                    else -> throw Exception()
                }
                val bos = ByteArrayOutputStream()
                bitmap.compress(Bitmap.CompressFormat.PNG, 0, bos)
                val bin = ByteArrayInputStream(bos.toByteArray())
                callback.onDataReady(bin)
            } catch (e: Exception) {
                e.printStackTrace()
                callback.onLoadFailed(e)
            }
        }
    }

    private fun mergeBitmaps(bitmaps: List<Bitmap>, rows: Int, columns: Int): Bitmap {
        val paint = Paint()
        val width = bitmaps[0].width
        val height = bitmaps[0].height
        val bitmap = Bitmap.createBitmap(width * columns, height * rows, Bitmap.Config.RGB_565)
        val canvas = Canvas(bitmap)
        var topOffset = 0F
        var leftOffset = 0F
        var position = 0
        for (i in 0 until rows) {
            for (j in 0 until columns) {
                canvas.drawBitmap(bitmaps[position], leftOffset, topOffset, paint)
                leftOffset += width
                position++
                if (position == bitmaps.size) return bitmap
            }
            topOffset += height
            leftOffset = 0F
        }
        return bitmap
    }
}