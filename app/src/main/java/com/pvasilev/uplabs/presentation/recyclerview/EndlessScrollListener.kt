package com.pvasilev.uplabs.presentation.recyclerview

import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class EndlessScrollListener constructor(private val loadMore: (totalItemCount: Int) -> Unit) :
    RecyclerView.OnScrollListener() {

    private var isLoading = false

    private var previousItemCount = 0

    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        val layoutManager = recyclerView.layoutManager!!
        val itemCount = layoutManager.itemCount
        val lastVisiblePosition = when (layoutManager) {
            is LinearLayoutManager -> layoutManager.findLastVisibleItemPosition()
            is GridLayoutManager -> layoutManager.findLastVisibleItemPosition()
            else -> 0
        }
        if (previousItemCount == 0) {
            previousItemCount = itemCount
        }
        if (isLoading && previousItemCount < itemCount) {
            isLoading = false
            previousItemCount = itemCount
        }
        if (!isLoading && lastVisiblePosition + LOAD_MORE_THRESHOLD > itemCount) {
            loadMore(itemCount)
            isLoading = true
        }
    }

    companion object {
        private const val LOAD_MORE_THRESHOLD = 2
    }
}