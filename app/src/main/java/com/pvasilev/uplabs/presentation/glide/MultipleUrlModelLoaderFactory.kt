package com.pvasilev.uplabs.presentation.glide

import com.bumptech.glide.load.model.ModelLoaderFactory
import com.bumptech.glide.load.model.MultiModelLoaderFactory
import com.pvasilev.uplabs.presentation.glide.MultipleUrlModelLoader
import java.io.InputStream

class MultipleUrlModelLoaderFactory : ModelLoaderFactory<List<*>, InputStream> {
    override fun build(multiFactory: MultiModelLoaderFactory) =
        MultipleUrlModelLoader()

    override fun teardown() {
    }
}