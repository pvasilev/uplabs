package com.pvasilev.uplabs.presentation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.aurelhubert.ahbottomnavigation.AHBottomNavigation
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationAdapter
import com.pvasilev.uplabs.R
import com.pvasilev.uplabs.Screens
import kotlinx.android.synthetic.main.fragment_main_flow.*
import ru.terrakok.cicerone.android.support.SupportAppScreen

class MainFlowFragment : Fragment(), AHBottomNavigation.OnTabSelectedListener, OnBackPressedListener {

    private val currentFragment: Fragment?
        get() = childFragmentManager.fragments.firstOrNull { !it.isHidden }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View =
        inflater.inflate(R.layout.fragment_main_flow, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        AHBottomNavigationAdapter(activity, R.menu.main_bottom_menu).setupWithBottomNavigation(bottomBar)
        bottomBar.titleState = AHBottomNavigation.TitleState.ALWAYS_HIDE
        bottomBar.setOnTabSelectedListener(this)
        selectTab(tabPosts)
    }

    override fun onTabSelected(position: Int, wasSelected: Boolean): Boolean {
        val tab = when (position) {
            0 -> tabPosts
            1 -> tabCollections
            2 -> tabLeaders
            else -> tabProfile
        }
        selectTab(tab)
        return true
    }

    private fun selectTab(tab: SupportAppScreen) {
        val newFragment = childFragmentManager.findFragmentByTag(tab.screenKey)
        childFragmentManager.beginTransaction().apply {
            if (newFragment == null) {
                add(R.id.mainContainer, tab.fragment, tab.screenKey)
            }
            currentFragment?.let {
                hide(it)
            }
            newFragment?.let {
                show(it)
            }
        }.commitNow()
    }

    override fun onBackPressed() {
        (currentFragment as? OnBackPressedListener)?.onBackPressed()
    }

    companion object {
        private val tabPosts = Screens.PostsFlowScreen

        private val tabCollections = Screens.CollectionsFlowScreen

        private val tabLeaders = Screens.LeadersFlowScreen

        private val tabProfile = Screens.ProfileFlowScreen("ramotion")
    }
}