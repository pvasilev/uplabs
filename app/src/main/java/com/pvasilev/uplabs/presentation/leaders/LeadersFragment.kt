package com.pvasilev.uplabs.presentation.leaders

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.pvasilev.uplabs.R
import com.pvasilev.uplabs.databinding.FragmentLeadersBinding
import com.pvasilev.uplabs.presentation.base.BaseFragment
import com.pvasilev.uplabs.presentation.LeadersAdapter
import com.pvasilev.uplabs.presentation.ViewModelFactory

class LeadersFragment : BaseFragment<LeadersViewModel>() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View =
        inflater.inflate(R.layout.fragment_leaders, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        FragmentLeadersBinding.bind(view).apply {
            recyclerView.apply {
                layoutManager = LinearLayoutManager(context)
                adapter = LeadersAdapter {}
            }
            leaders = vm.leaders
            setLifecycleOwner(this@LeadersFragment)
        }
    }

    override fun provideViewModel() = ViewModelProviders.of(this, ViewModelFactory())
        .get(LeadersViewModel::class.java)
}