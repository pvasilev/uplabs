package com.pvasilev.uplabs.presentation.followings

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.pvasilev.uplabs.R
import com.pvasilev.uplabs.databinding.FragmentUsersBinding
import com.pvasilev.uplabs.di.DI
import com.pvasilev.uplabs.presentation.base.BaseFragment
import com.pvasilev.uplabs.presentation.FollowingsAdapter
import com.pvasilev.uplabs.presentation.ViewModelFactory
import com.pvasilev.uplabs.presentation.argument
import toothpick.Toothpick
import toothpick.config.Module

class FollowingsFragment : BaseFragment<FollowingsViewModel>() {

    private val nickname by argument(ARG_NICKNAME, "")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        prepareScope()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View =
        inflater.inflate(R.layout.fragment_users, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        FragmentUsersBinding.bind(view).apply {
            recyclerView.apply {
                adapter = FollowingsAdapter(vm::onUserClicked)
                layoutManager = LinearLayoutManager(context)
            }
            users = vm.followings
            setLifecycleOwner(this@FollowingsFragment)
        }
    }

    override fun provideViewModel() = ViewModelProviders.of(this, ViewModelFactory())
        .get(FollowingsViewModel::class.java)

    private fun prepareScope() {
        val scope = Toothpick.openScopes(DI.PROFILE_SCOPE, DI.FOLLOWINGS_SCOPE)
        val module = Module().apply {
            bind(String::class.java).toInstance(nickname)
        }
        scope.installModules(module)
    }

    companion object {
        private const val ARG_NICKNAME = "arg_nickname"

        fun newInstance(nickname: String) = FollowingsFragment().apply {
            arguments = Bundle().apply {
                putString(ARG_NICKNAME, nickname)
            }
        }
    }
}