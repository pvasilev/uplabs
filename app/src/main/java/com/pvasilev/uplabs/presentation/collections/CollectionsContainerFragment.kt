package com.pvasilev.uplabs.presentation.collections

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentPagerAdapter
import com.pvasilev.uplabs.R
import com.pvasilev.uplabs.Screens
import kotlinx.android.synthetic.main.fragment_collections_container.*

class CollectionsContainerFragment : Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View =
        inflater.inflate(R.layout.fragment_collections_container, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        viewPager.adapter = CollectionsPagerAdapter()
    }

    inner class CollectionsPagerAdapter : FragmentPagerAdapter(childFragmentManager) {
        override fun getItem(position: Int) =
            when (position) {
                0 -> Screens.PopularCollectionsScreen.fragment
                1 -> Screens.SavedCollectionsScreen.fragment
                else -> throw IllegalArgumentException()
            }

        override fun getPageTitle(position: Int) =
            when (position) {
                0 -> resources.getString(R.string.tab_collections_popular)
                1 -> resources.getString(R.string.tab_collections_saved)
                else -> throw IllegalArgumentException()
            }

        override fun getCount() = 2
    }
}