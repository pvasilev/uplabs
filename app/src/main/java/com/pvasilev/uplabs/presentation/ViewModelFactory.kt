package com.pvasilev.uplabs.presentation

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.pvasilev.uplabs.di.DI
import com.pvasilev.uplabs.presentation.collections.PopularCollectionsViewModel
import com.pvasilev.uplabs.presentation.collections.SavedCollectionsViewModel
import com.pvasilev.uplabs.presentation.followers.FollowersViewModel
import com.pvasilev.uplabs.presentation.followings.FollowingsViewModel
import com.pvasilev.uplabs.presentation.leaders.LeadersViewModel
import com.pvasilev.uplabs.presentation.post.PostViewModel
import com.pvasilev.uplabs.presentation.posts.PostsViewModel
import com.pvasilev.uplabs.presentation.profile.ProfileViewModel
import toothpick.Toothpick

class ViewModelFactory : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        val scope = when (modelClass) {
            PostsViewModel::class.java -> Toothpick.openScope(DI.POSTS_SCOPE)
            PostViewModel::class.java -> Toothpick.openScope(DI.POST_SCOPE)
            PopularCollectionsViewModel::class.java -> Toothpick.openScope(DI.COLLECTIONS_SCOPE)
            SavedCollectionsViewModel::class.java -> Toothpick.openScope(DI.COLLECTIONS_SCOPE)
            LeadersViewModel::class.java -> Toothpick.openScope(DI.LEADERS_SCOPE)
            ProfileViewModel::class.java -> Toothpick.openScope(DI.PROFILE_SCOPE)
            FollowersViewModel::class.java -> Toothpick.openScope(DI.FOLLOWERS_SCOPE)
            FollowingsViewModel::class.java -> Toothpick.openScope(DI.FOLLOWINGS_SCOPE)
            else -> throw IllegalArgumentException()
        }
        return scope.getInstance(modelClass) as T
    }
}