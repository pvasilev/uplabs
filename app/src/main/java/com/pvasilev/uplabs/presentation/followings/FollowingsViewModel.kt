package com.pvasilev.uplabs.presentation.followings

import androidx.lifecycle.MutableLiveData
import com.pvasilev.uplabs.Screens
import com.pvasilev.uplabs.data.model.User
import com.pvasilev.uplabs.di.DI
import com.pvasilev.uplabs.domain.GetFollowingsUseCase
import com.pvasilev.uplabs.presentation.base.BaseViewModel
import kotlinx.coroutines.launch
import ru.terrakok.cicerone.Router
import toothpick.Toothpick
import javax.inject.Inject

class FollowingsViewModel @Inject constructor(
    router: Router,
    private val nickname: String,
    private val getFollowings: GetFollowingsUseCase
) : BaseViewModel(router) {

    val followings: MutableLiveData<List<User>> = MutableLiveData()

    init {
        launch {
            followings.postValue(getFollowings(nickname))
        }
    }

    fun onUserClicked(user: User) {
        router.navigateTo(Screens.ProfileScreen(user.nickname))
    }

    override fun onCleared() {
        super.onCleared()
        Toothpick.closeScope(DI.FOLLOWINGS_SCOPE)
    }
}