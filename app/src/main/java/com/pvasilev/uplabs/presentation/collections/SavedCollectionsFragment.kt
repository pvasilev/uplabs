package com.pvasilev.uplabs.presentation.collections

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.pvasilev.uplabs.R
import com.pvasilev.uplabs.databinding.FragmentCollectionsBinding
import com.pvasilev.uplabs.presentation.base.BaseFragment
import com.pvasilev.uplabs.presentation.CollectionsAdapter
import com.pvasilev.uplabs.presentation.recyclerview.MarginItemDecoration
import com.pvasilev.uplabs.presentation.ViewModelFactory

class SavedCollectionsFragment : BaseFragment<SavedCollectionsViewModel>() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View =
        inflater.inflate(R.layout.fragment_collections, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        FragmentCollectionsBinding.bind(view).apply {
            recyclerView.apply {
                adapter = CollectionsAdapter(vm::onCollectionClicked)
                layoutManager = LinearLayoutManager(context)
                addItemDecoration(
                    MarginItemDecoration(
                        resources.getDimensionPixelSize(
                            R.dimen.margin_medium
                        )
                    )
                )
            }
            collections = vm.collections
            setLifecycleOwner(this@SavedCollectionsFragment)
        }
    }

    override fun provideViewModel() = ViewModelProviders.of(this, ViewModelFactory())
        .get(SavedCollectionsViewModel::class.java)
}