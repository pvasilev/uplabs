package com.pvasilev.uplabs.presentation

interface OnBackPressedListener {
    fun onBackPressed()
}