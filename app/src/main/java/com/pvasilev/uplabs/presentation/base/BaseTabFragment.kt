package com.pvasilev.uplabs.presentation.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.pvasilev.uplabs.R
import com.pvasilev.uplabs.di.DI
import com.pvasilev.uplabs.di.NavigationModule
import com.pvasilev.uplabs.presentation.OnBackPressedListener
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router
import ru.terrakok.cicerone.Screen
import ru.terrakok.cicerone.android.support.SupportAppNavigator
import toothpick.Toothpick
import javax.inject.Inject

abstract class BaseTabFragment : Fragment(), OnBackPressedListener {

    abstract val scope: String

    abstract val screen: Screen

    @Inject
    lateinit var router: Router

    @Inject
    lateinit var navigatorHolder: NavigatorHolder

    private val navigator by lazy { SupportAppNavigator(activity, childFragmentManager, R.id.tabContainer) }

    private val currentFragment: Fragment?
        get() = childFragmentManager.findFragmentById(R.id.tabContainer)

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View =
        inflater.inflate(R.layout.fragment_tab_flow, container, false)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Toothpick.openScopes(DI.APP_SCOPE, scope)
            .also { it.installModules(NavigationModule()) }
            .also { Toothpick.inject(this, it) }
        if (savedInstanceState == null) {
            router.navigateTo(screen)
        }
    }

    override fun onResume() {
        super.onResume()
        navigatorHolder.setNavigator(navigator)
    }

    override fun onPause() {
        super.onPause()
        navigatorHolder.removeNavigator()
    }

    override fun onBackPressed() {
        (currentFragment as? OnBackPressedListener)?.onBackPressed()
    }
}