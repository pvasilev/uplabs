package com.pvasilev.uplabs.presentation.followers

import androidx.lifecycle.MutableLiveData
import com.pvasilev.uplabs.Screens
import com.pvasilev.uplabs.data.model.User
import com.pvasilev.uplabs.di.DI
import com.pvasilev.uplabs.domain.GetFollowersUseCase
import com.pvasilev.uplabs.presentation.base.BaseViewModel
import kotlinx.coroutines.launch
import ru.terrakok.cicerone.Router
import toothpick.Toothpick
import javax.inject.Inject

class FollowersViewModel @Inject constructor(
    router: Router,
    private val nickname: String,
    private val getFollowers: GetFollowersUseCase
) : BaseViewModel(router) {

    val followers: MutableLiveData<List<User>> = MutableLiveData()

    init {
        launch {
            followers.postValue(getFollowers(nickname))
        }
    }

    fun onUserClicked(user: User) {
        router.navigateTo(Screens.ProfileScreen(user.nickname))
    }

    override fun onCleared() {
        super.onCleared()
        Toothpick.closeScope(DI.FOLLOWERS_SCOPE)
    }
}