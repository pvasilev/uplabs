package com.pvasilev.uplabs.presentation.recyclerview

typealias OnItemClickListener<T> = (T) -> Unit