package com.pvasilev.uplabs.presentation.base

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.pvasilev.uplabs.presentation.recyclerview.OnItemClickListener

abstract class BaseAdapter<T : Any>(
    private val onItemClickListener: OnItemClickListener<T>? = null,
    diffCallback: DiffUtil.ItemCallback<T>
) : ListAdapter<T, BaseViewHolder>(diffCallback) {
    @get:LayoutRes
    abstract val layoutResId: Int

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = DataBindingUtil.inflate<ViewDataBinding>(inflater, layoutResId, parent, false)
        return BaseViewHolder(binding)
    }

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        val item = getItem(position)
        holder.itemView.setOnClickListener { onItemClickListener?.invoke(item) }
        holder.bind(item)
    }
}