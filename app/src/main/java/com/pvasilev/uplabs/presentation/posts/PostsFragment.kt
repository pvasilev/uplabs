package com.pvasilev.uplabs.presentation.posts

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.pvasilev.uplabs.R
import com.pvasilev.uplabs.databinding.FragmentPostsBinding
import com.pvasilev.uplabs.presentation.base.BaseFragment
import com.pvasilev.uplabs.presentation.recyclerview.MarginItemDecoration
import com.pvasilev.uplabs.presentation.PostsAdapter
import com.pvasilev.uplabs.presentation.ViewModelFactory

class PostsFragment : BaseFragment<PostsViewModel>() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View =
        inflater.inflate(R.layout.fragment_posts, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        FragmentPostsBinding.bind(view).apply {
            recyclerView.apply {
                layoutManager = LinearLayoutManager(context)
                adapter = PostsAdapter(vm::onPostClicked)
                addItemDecoration(
                    MarginItemDecoration(
                        resources.getDimensionPixelSize(R.dimen.margin_medium),
                        resources.getDimensionPixelSize(R.dimen.margin_small)
                    )
                )
            }
            posts = vm.posts
            setLifecycleOwner(this@PostsFragment)
        }
    }

    override fun provideViewModel() = ViewModelProviders.of(this, ViewModelFactory())
        .get(PostsViewModel::class.java)
}