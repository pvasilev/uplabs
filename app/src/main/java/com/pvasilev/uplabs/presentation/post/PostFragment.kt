package com.pvasilev.uplabs.presentation.post

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.pvasilev.uplabs.R
import com.pvasilev.uplabs.databinding.FragmentPostBinding
import com.pvasilev.uplabs.di.DI
import com.pvasilev.uplabs.presentation.*
import com.pvasilev.uplabs.presentation.base.BaseFragment
import com.pvasilev.uplabs.presentation.recyclerview.MarginItemDecoration
import toothpick.Toothpick
import toothpick.config.Module

class PostFragment : BaseFragment<PostViewModel>() {

    private val postId: String by argument(ARG_POST_ID, "")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        prepareScope()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View =
        inflater.inflate(R.layout.fragment_post, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        FragmentPostBinding.bind(view).apply {
            rvPosts.adapter = RelatedAdapter(vm::onPostClicked)
            rvPosts.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
            rvPosts.addItemDecoration(
                MarginItemDecoration(
                    resources.getDimensionPixelSize(
                        R.dimen.margin_small
                    ), 0
                )
            )
            rvComments.adapter = CommentsAdapter()
            rvComments.layoutManager = LinearLayoutManager(context)
            post = vm.post
            posts = vm.posts
            comments = vm.comments
            setLifecycleOwner(this@PostFragment)
        }
    }

    override fun provideViewModel() =
        ViewModelProviders.of(this, ViewModelFactory()).get(PostViewModel::class.java)

    private fun prepareScope() {
        val scope = Toothpick.openScopes(DI.APP_SCOPE, DI.POST_SCOPE)
        val module = Module().apply {
            bind(String::class.java).toInstance(postId)
        }
        scope.installModules(module)
    }

    companion object {
        private const val ARG_POST_ID = "arg_post_id"

        fun newInstance(postId: String) = PostFragment().apply {
            arguments = Bundle().apply {
                putString(ARG_POST_ID, postId)
            }
        }
    }
}