package com.pvasilev.uplabs.presentation

import android.os.Bundle
import com.pvasilev.uplabs.Screens
import com.pvasilev.uplabs.di.DI
import com.pvasilev.uplabs.presentation.base.BaseTabFragment
import ru.terrakok.cicerone.Screen

class PostsFlowFragment : BaseTabFragment() {
    override val scope: String
        get() = DI.POSTS_SCOPE

    override val screen: Screen
        get() = Screens.PostsScreen
}

class CollectionsFlowFragment : BaseTabFragment() {
    override val scope: String
        get() = DI.COLLECTIONS_SCOPE

    override val screen: Screen
        get() = Screens.CollectionsContainerScreen
}

class LeadersFlowFragment : BaseTabFragment() {
    override val scope: String
        get() = DI.LEADERS_SCOPE

    override val screen: Screen
        get() = Screens.LeadersScreen
}

class ProfileFlowFragment : BaseTabFragment() {
    override val scope: String
        get() = DI.PROFILE_SCOPE

    override val screen: Screen
        get() = Screens.ProfileScreen(nickname)

    private val nickname by argument(ARG_NICKNAME, "")

    companion object {
        private const val ARG_NICKNAME = "arg_nickname"

        fun newInstance(nickname: String) = ProfileFlowFragment().apply {
            arguments = Bundle().apply {
                putString(ARG_NICKNAME, nickname)
            }
        }
    }
}
