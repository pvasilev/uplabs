package com.pvasilev.uplabs.presentation.post

import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import com.pvasilev.uplabs.Screens
import com.pvasilev.uplabs.data.model.Comment
import com.pvasilev.uplabs.data.model.Post
import com.pvasilev.uplabs.di.DI
import com.pvasilev.uplabs.domain.GetCommentsUseCase
import com.pvasilev.uplabs.domain.GetPostUseCase
import com.pvasilev.uplabs.domain.GetRelatedPostsUseCase
import com.pvasilev.uplabs.presentation.base.BaseViewModel
import kotlinx.coroutines.launch
import ru.terrakok.cicerone.Router
import toothpick.Toothpick
import javax.inject.Inject

class PostViewModel @Inject constructor(
    router: Router,
    postId: String,
    getPost: GetPostUseCase,
    getRelatedPosts: GetRelatedPostsUseCase,
    getComments: GetCommentsUseCase
) : BaseViewModel(router) {

    val post: MutableLiveData<Post> = MutableLiveData()

    val posts: MutableLiveData<List<Post>> = MediatorLiveData<List<Post>>().apply {
        addSource(post) {
            launch {
                postValue(getRelatedPosts(it.subcategoryUrl))
            }
        }
    }

    val comments: MutableLiveData<List<Comment>> = MutableLiveData()

    init {
        post.postValue(getPost(postId))
        launch {
            comments.postValue(getComments(postId))
        }
    }

    override fun onCleared() {
        super.onCleared()
        Toothpick.closeScope(DI.POST_SCOPE)
    }

    fun onPostClicked(post: Post) {
        router.navigateTo(Screens.PostScreen(post.id))
    }
}