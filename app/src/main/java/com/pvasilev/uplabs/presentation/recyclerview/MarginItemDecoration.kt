package com.pvasilev.uplabs.presentation.recyclerview

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView

class MarginItemDecoration(
    private val marginLeft: Int,
    private val marginTop: Int,
    private val marginRight: Int,
    private val marginBottom: Int
) : RecyclerView.ItemDecoration() {

    constructor(margin: Int) : this(margin, margin, margin, margin)

    constructor(marginHorizontal: Int, marginVertical: Int) : this(
        marginHorizontal,
        marginVertical,
        marginHorizontal,
        marginVertical
    )

    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
        outRect.left = marginLeft
        outRect.top = marginTop
        outRect.right = marginRight
        outRect.bottom = marginBottom
    }
}