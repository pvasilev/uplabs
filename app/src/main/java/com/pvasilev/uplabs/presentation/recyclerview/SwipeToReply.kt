package com.pvasilev.uplabs.presentation.recyclerview

import android.content.Context
import android.graphics.Canvas
import android.graphics.drawable.ColorDrawable
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.pvasilev.uplabs.R

abstract class SwipeToReply(private val context: Context) :
    ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.RIGHT) {

    override fun onMove(
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder,
        target: RecyclerView.ViewHolder
    ) = false

    override fun onChildDraw(
        canvas: Canvas,
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder,
        dX: Float,
        dY: Float,
        actionState: Int,
        isCurrentlyActive: Boolean
    ) {
        val colorWhite = ContextCompat.getColor(context, android.R.color.white)
        val colorPrimary = ContextCompat.getColor(context, R.color.colorPrimary)
        val marginMedium = context.resources.getDimensionPixelOffset(R.dimen.margin_medium)
        val itemHeight = viewHolder.itemView.height
        val itemTop = viewHolder.itemView.top
        val itemBottom = viewHolder.itemView.bottom
        val background = ColorDrawable(colorPrimary)
        val icon = ContextCompat.getDrawable(context, R.drawable.ic_reply) ?: return
        background.setBounds(0, itemTop, dX.toInt(), itemBottom)
        background.draw(canvas)
        val iconTop = itemTop + (itemHeight - icon.intrinsicHeight) / 2
        val iconBottom = iconTop + icon.intrinsicHeight
        icon.setTint(colorWhite)
        icon.setBounds(marginMedium, iconTop, marginMedium + icon.intrinsicWidth, iconBottom)
        icon.draw(canvas)
        super.onChildDraw(canvas, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)
    }

    override fun getSwipeThreshold(viewHolder: RecyclerView.ViewHolder): Float {
        val marginMedium = context.resources.getDimensionPixelOffset(R.dimen.margin_medium)
        val iconWidth = ContextCompat.getDrawable(context, R.drawable.ic_reply)?.intrinsicWidth ?: 0
        return (iconWidth + marginMedium * 2.0F) / viewHolder.itemView.width
    }
}