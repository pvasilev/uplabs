package com.pvasilev.uplabs.presentation.base

import androidx.lifecycle.ViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import ru.terrakok.cicerone.Router
import kotlin.coroutines.CoroutineContext

open class BaseViewModel(protected val router: Router) : ViewModel(), CoroutineScope {
    override val coroutineContext: CoroutineContext
        get() = job + Dispatchers.Main

    private val job = Job()

    override fun onCleared() {
        super.onCleared()
        job.cancel()
    }

    fun onBackPressed() {
        router.exit()
    }
}