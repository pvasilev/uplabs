package com.pvasilev.uplabs.presentation.posts

import androidx.lifecycle.MutableLiveData
import com.pvasilev.uplabs.Screens
import com.pvasilev.uplabs.data.model.Post
import com.pvasilev.uplabs.domain.GetPopularPostsUseCase
import com.pvasilev.uplabs.presentation.base.BaseViewModel
import kotlinx.coroutines.launch
import ru.terrakok.cicerone.Router
import javax.inject.Inject

class PostsViewModel @Inject constructor(
    router: Router,
    private val getPopularPosts: GetPopularPostsUseCase
) : BaseViewModel(router) {

    val posts: MutableLiveData<List<Post>> = MutableLiveData()

    init {
        launch {
            posts.postValue(getPopularPosts())
        }
    }

    fun onPostClicked(post: Post) {
        router.navigateTo(Screens.PostScreen(post.id))
    }
}