package com.pvasilev.uplabs.presentation.leaders

import androidx.lifecycle.MutableLiveData
import com.pvasilev.uplabs.data.model.User
import com.pvasilev.uplabs.domain.GetLeadersUseCase
import com.pvasilev.uplabs.presentation.base.BaseViewModel
import kotlinx.coroutines.launch
import ru.terrakok.cicerone.Router
import javax.inject.Inject

class LeadersViewModel @Inject constructor(
    router: Router,
    private val getLeaders: GetLeadersUseCase
) : BaseViewModel(router) {

    val leaders: MutableLiveData<List<User>> = MutableLiveData()

    init {
        launch {
            leaders.postValue(getLeaders())
        }
    }
}