package com.pvasilev.uplabs.presentation.glide

import com.bumptech.glide.load.Options
import com.bumptech.glide.load.model.ModelLoader
import com.bumptech.glide.signature.ObjectKey
import java.io.InputStream

class MultipleUrlModelLoader : ModelLoader<List<*>, InputStream> {
    override fun buildLoadData(
        model: List<*>,
        width: Int,
        height: Int,
        options: Options?
    ) = ModelLoader.LoadData(ObjectKey(model), MultipleUrlDataFetcher(model))

    override fun handles(model: List<*>?) = true
}