package com.pvasilev.uplabs.presentation.base

import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.pvasilev.uplabs.BR

class BaseViewHolder(private val binding: ViewDataBinding) : RecyclerView.ViewHolder(binding.root) {
    fun bind(item: Any) {
        binding.setVariable(BR.item, item)
        binding.setVariable(BR.position, adapterPosition)
        binding.executePendingBindings()
    }
}