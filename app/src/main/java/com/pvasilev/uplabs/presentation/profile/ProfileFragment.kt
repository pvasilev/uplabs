package com.pvasilev.uplabs.presentation.profile

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import com.pvasilev.uplabs.R
import com.pvasilev.uplabs.databinding.FragmentProfileBinding
import com.pvasilev.uplabs.di.DI
import com.pvasilev.uplabs.presentation.*
import com.pvasilev.uplabs.presentation.base.BaseFragment
import toothpick.Toothpick
import toothpick.config.Module

class ProfileFragment : BaseFragment<ProfileViewModel>() {

    private val nickname by argument(ARG_NICKNAME, "")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        prepareScope()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View =
        inflater.inflate(R.layout.fragment_profile, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        FragmentProfileBinding.bind(view).apply {
            statsContainer.btnFollowers.setOnClickListener { vm.onFollowersClicked() }
            statsContainer.btnFollowing.setOnClickListener { vm.onFollowingClicked() }
            recyclerView.apply {
                adapter = GalleryAdapter(vm::onPostClicked)
                layoutManager = GridLayoutManager(context, 3)
            }
            viewmodel = vm
            setLifecycleOwner(this@ProfileFragment)
        }
    }

    override fun provideViewModel() = ViewModelProviders.of(this, ViewModelFactory())
        .get(ProfileViewModel::class.java)

    private fun prepareScope() {
        val scope = Toothpick.openScopes(DI.APP_SCOPE, DI.PROFILE_SCOPE)
        val module = Module().apply {
            bind(String::class.java).toInstance(nickname)
        }
        scope.installModules(module)
    }

    companion object {
        private const val ARG_NICKNAME = "arg_nickname"

        fun newInstance(nickname: String) = ProfileFragment().apply {
            arguments = Bundle().apply {
                putString(ARG_NICKNAME, nickname)
            }
        }
    }
}