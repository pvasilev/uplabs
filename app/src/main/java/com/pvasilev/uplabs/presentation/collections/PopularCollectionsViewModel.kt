package com.pvasilev.uplabs.presentation.collections

import androidx.lifecycle.MutableLiveData
import com.pvasilev.uplabs.data.model.Collection
import com.pvasilev.uplabs.domain.GetCollectionsUseCase
import com.pvasilev.uplabs.presentation.base.BaseViewModel
import kotlinx.coroutines.launch
import ru.terrakok.cicerone.Router
import javax.inject.Inject

class PopularCollectionsViewModel @Inject constructor(
    router: Router,
    private val getCollections: GetCollectionsUseCase
) : BaseViewModel(router) {

    val collections: MutableLiveData<List<Collection>> = MutableLiveData()

    init {
        launch {
            collections.postValue(getCollections())
        }
    }

    fun onCollectionClicked(collection: Collection) {
    }
}