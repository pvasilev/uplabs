package com.pvasilev.uplabs.di

import com.pvasilev.uplabs.data.LocalDateTimeJsonAdapter
import com.serjltt.moshi.adapters.Wrapped
import com.squareup.moshi.Moshi
import org.threeten.bp.LocalDateTime
import javax.inject.Inject
import javax.inject.Provider

class MoshiProvider @Inject constructor() : Provider<Moshi> {
    override fun get(): Moshi =
        Moshi.Builder()
            .add(LocalDateTime::class.java, LocalDateTimeJsonAdapter())
            .add(Wrapped.ADAPTER_FACTORY)
            .build()
}