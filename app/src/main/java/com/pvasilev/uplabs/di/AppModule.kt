package com.pvasilev.uplabs.di

import com.pvasilev.uplabs.data.api.UplabsApi
import com.pvasilev.uplabs.data.repository.PostRepository
import com.pvasilev.uplabs.data.repository.UserRepository
import com.squareup.moshi.Moshi
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import toothpick.config.Module

class AppModule : Module() {
    init {
        bind(Moshi::class.java).toProvider(MoshiProvider::class.java)
        bind(OkHttpClient::class.java).toProvider(OkHttpClientProvider::class.java)
        bind(Retrofit::class.java).toProvider(RetrofitProvider::class.java)
        bind(UplabsApi::class.java).toProvider(UplabsApiProvider::class.java)
        bind(UserRepository::class.java).singletonInScope()
        bind(PostRepository::class.java).singletonInScope()
    }
}