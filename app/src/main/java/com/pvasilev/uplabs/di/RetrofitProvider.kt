package com.pvasilev.uplabs.di

import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.pvasilev.uplabs.data.LeadersConverterFactory
import com.squareup.moshi.Moshi
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import javax.inject.Inject
import javax.inject.Provider

class RetrofitProvider @Inject constructor(private val okHttpClient: OkHttpClient, private val moshi: Moshi) : Provider<Retrofit> {
    override fun get(): Retrofit =
        Retrofit.Builder()
            .baseUrl("https://www.uplabs.com/")
            .client(okHttpClient)
            .addConverterFactory(LeadersConverterFactory())
            .addConverterFactory(MoshiConverterFactory.create(moshi))
            .addCallAdapterFactory(CoroutineCallAdapterFactory())
            .build()
}