package com.pvasilev.uplabs.di

object DI {
    const val APP_SCOPE = "app scope"
    const val POSTS_SCOPE = "posts scope"
    const val POST_SCOPE = "post scope"
    const val COLLECTIONS_SCOPE = "collections scope"
    const val LEADERS_SCOPE = "leaders scope"
    const val PROFILE_SCOPE = "profile scope"
    const val FOLLOWERS_SCOPE = "followers scope"
    const val FOLLOWINGS_SCOPE = "followings scope"
}