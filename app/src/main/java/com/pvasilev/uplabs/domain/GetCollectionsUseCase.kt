package com.pvasilev.uplabs.domain

import com.pvasilev.uplabs.data.repository.CollectionRepository
import javax.inject.Inject

class GetCollectionsUseCase @Inject constructor(private val collectionRepository: CollectionRepository) {
    suspend operator fun invoke() = collectionRepository.getCollections()
}