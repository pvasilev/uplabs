package com.pvasilev.uplabs.domain

import com.pvasilev.uplabs.data.repository.UserRepository
import javax.inject.Inject

class GetFollowersUseCase @Inject constructor(private val userRepository: UserRepository) {
    suspend operator fun invoke(nickname: String) = userRepository.getFollowers(nickname)
}