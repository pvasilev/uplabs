package com.pvasilev.uplabs.domain

import com.pvasilev.uplabs.data.repository.PostRepository
import javax.inject.Inject

class GetPopularPostsUseCase @Inject constructor(private val postRepository: PostRepository) {
    suspend operator fun invoke() = postRepository.getPosts()
}