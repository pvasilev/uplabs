package com.pvasilev.uplabs.domain

import com.pvasilev.uplabs.data.repository.CollectionRepository
import javax.inject.Inject

class GetMyCollectionsUseCase @Inject constructor(private val collectionRepository: CollectionRepository) {
    suspend operator fun invoke() = collectionRepository.getMyCollections()
}