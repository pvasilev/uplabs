package com.pvasilev.uplabs.domain

import com.pvasilev.uplabs.data.repository.PostRepository
import javax.inject.Inject

class GetCommentsUseCase @Inject constructor(private val postRepository: PostRepository) {
    suspend operator fun invoke(postId: String) = postRepository.getComments(postId)
}