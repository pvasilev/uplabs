package com.pvasilev.uplabs.domain

import com.pvasilev.uplabs.data.repository.PostRepository
import javax.inject.Inject

class GetRelatedPostsUseCase @Inject constructor(private val postRepository: PostRepository) {
    suspend operator fun invoke(subcategoryUrl: String) = postRepository.getRelatedPosts("$subcategoryUrl.json")
}