package com.pvasilev.uplabs.domain

import com.pvasilev.uplabs.data.repository.PostRepository
import javax.inject.Inject

class GetUserPostsUseCase @Inject constructor(private val postRepository: PostRepository) {
    suspend operator fun invoke(nickname: String) = postRepository.getPosts(nickname)
}