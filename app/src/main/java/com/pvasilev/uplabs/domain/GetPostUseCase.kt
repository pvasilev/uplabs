package com.pvasilev.uplabs.domain

import com.pvasilev.uplabs.data.repository.PostRepository
import javax.inject.Inject

class GetPostUseCase @Inject constructor(private val postRepository: PostRepository) {
    operator fun invoke(postId: String) = postRepository.getPost(postId)
}