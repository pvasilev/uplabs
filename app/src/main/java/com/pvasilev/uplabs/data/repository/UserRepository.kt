package com.pvasilev.uplabs.data.repository

import com.pvasilev.uplabs.data.api.UplabsApi
import com.pvasilev.uplabs.data.model.User
import javax.inject.Inject

class UserRepository @Inject constructor(private val api: UplabsApi) {
    suspend fun getFollowers(nickname: String) = api.getFollowers(nickname).await()

    suspend fun getFollowings(nickname: String) = api.getFollowings(nickname).await()

    suspend fun getLeaders(): List<User> {
        val nicknames = api.getLeaders().await()
        return nicknames
            .take(20)
            .map { api.getPosts(it).await().first().author }
    }
}