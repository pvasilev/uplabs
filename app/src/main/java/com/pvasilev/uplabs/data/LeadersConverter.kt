package com.pvasilev.uplabs.data

import okhttp3.ResponseBody
import org.jsoup.Jsoup
import retrofit2.Converter

class LeadersConverter : Converter<ResponseBody, List<String>> {
    override fun convert(value: ResponseBody): List<String> {
        val document = Jsoup.parse(value.string())
        return document.getElementsByClass("leaderboard__user-avatar")
            .map { it.attr("alt") }
    }
}