package com.pvasilev.uplabs.data.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import org.threeten.bp.LocalDateTime

@JsonClass(generateAdapter = true)
data class Comment(
    val id: Int,
    val body: String,
    @Json(name = "created_at") val createdAt: LocalDateTime,
    val user: User
)