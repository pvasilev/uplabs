package com.pvasilev.uplabs.data.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class PostTeaser(@Json(name = "teaser_url") val teaserUrl: String)