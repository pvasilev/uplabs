package com.pvasilev.uplabs.data.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import org.threeten.bp.LocalDateTime

@JsonClass(generateAdapter = true)
data class Post(
    val id: String,
    val name: String,
    @Json(name = "description_without_html") val description: String?,
    @Json(name = "preview_url") val previewUrl: String,
    @Json(name = "category_name") val category: String,
    @Json(name = "subcategory_path") val subcategoryUrl: String,
    @Json(name = "serialized_submitter") val author: User,
    @Json(name = "showcased_at") val showcasedAt: LocalDateTime,
    val animated: Boolean,
    @Json(name = "paid_post") val premium: Boolean,
    @Json(name = "view_count") val views: Int,
    val points: Int
)