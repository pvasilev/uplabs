package com.pvasilev.uplabs.data.api

import com.pvasilev.uplabs.data.model.Collection
import com.pvasilev.uplabs.data.model.Comment
import com.pvasilev.uplabs.data.model.Post
import com.pvasilev.uplabs.data.model.User
import com.serjltt.moshi.adapters.Wrapped
import kotlinx.coroutines.Deferred
import retrofit2.http.*

interface UplabsApi {
    @GET("all.json")
    fun getPosts(): Deferred<List<Post>>

    @GET("users/{nickname}/showcases.json")
    fun getPosts(@Path("nickname") nickname: String): Deferred<List<Post>>

    @GET
    fun getRelatedPosts(@Url subcategoryUrl: String): Deferred<List<Post>>

    @GET("users/{nickname}/followers.json")
    fun getFollowers(@Path("nickname") nickname: String): Deferred<List<User>>

    @GET("users/{nickname}/followings.json")
    fun getFollowings(@Path("nickname") nickname: String): Deferred<List<User>>

    @GET("collections.json")
    fun getCollections(): Deferred<List<Collection>>

    @GET("collections/my_collections.json")
    fun getMyCollections(): Deferred<List<Collection>>

    @GET("collections/{collection_id}.json")
    fun getPostsForCollection(@Path("collection_id") collectionId: Int): Deferred<List<Post>>

    @PUT("collections/{collection_id}/collect")
    @Wrapped(path = ["collection"])
    fun collect(@Path("collection_id") collectionId: Int, @Query("post_id") postId: Int): Deferred<Collection>

    @DELETE("collections/{collection_id}/uncollect")
    @Wrapped(path = ["collection"])
    fun uncollect(@Path("collection_id") collectionId: Int, @Query("post_id") postId: Int): Deferred<Collection>

    @GET("leaderboards")
    fun getLeaders(): Deferred<List<String>>

    @GET("comments.json?commentable_type=post")
    @Wrapped(path = ["comments"])
    fun getComments(@Query("commentable_id") postId: String): Deferred<List<Comment>>
}