package com.pvasilev.uplabs.data.repository

import com.pvasilev.uplabs.data.api.UplabsApi
import javax.inject.Inject

class CollectionRepository @Inject constructor(private val api: UplabsApi) {
    suspend fun getCollections() = api.getCollections().await()

    suspend fun getMyCollections() = api.getMyCollections().await()
}