package com.pvasilev.uplabs.data

import com.squareup.moshi.Types
import okhttp3.ResponseBody
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.http.GET
import java.lang.reflect.Type

class LeadersConverterFactory : Converter.Factory() {
    override fun responseBodyConverter(
        type: Type,
        annotations: Array<Annotation>,
        retrofit: Retrofit
    ): Converter<ResponseBody, *>? {
        return if ((annotations[0] as? GET)?.value == "leaderboards" && type == Types.newParameterizedType(List::class.java, String::class.java)) {
            LeadersConverter()
        } else {
            null
        }
    }
}