package com.pvasilev.uplabs.data.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Collection(
    val id: Int,
    val name: String,
    val description: String?,
    @Json(name = "post_ids") val postIds: List<Int>,
    val user: User,
    @Json(name = "popular_posts") val popularPosts: List<PostTeaser>,
    @Json(name = "posts_count") val posts: Int,
    @Json(name = "followers_count") val followers: Int
)