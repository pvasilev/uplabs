package com.pvasilev.uplabs.data

import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.JsonReader
import com.squareup.moshi.JsonWriter
import org.threeten.bp.LocalDateTime
import org.threeten.bp.format.DateTimeFormatter

class LocalDateTimeJsonAdapter : JsonAdapter<LocalDateTime>() {
    override fun fromJson(reader: JsonReader): LocalDateTime =
        LocalDateTime.parse(reader.nextString(), DateTimeFormatter.ISO_DATE_TIME)

    override fun toJson(writer: JsonWriter, value: LocalDateTime?) {
        writer.value(value?.format(DateTimeFormatter.ISO_DATE_TIME))
    }
}