package com.pvasilev.uplabs.data.repository

import com.pvasilev.uplabs.data.api.UplabsApi
import com.pvasilev.uplabs.data.model.Post
import javax.inject.Inject

class PostRepository @Inject constructor(private val api: UplabsApi) {

    private val postById = mutableMapOf<String, Post>()

    suspend fun getPosts(nickname: String): List<Post> {
        val posts = api.getPosts(nickname).await()
        postById.putAll(posts.map { it.id to it })
        return posts
    }

    suspend fun getPosts(): List<Post> {
        val posts = api.getPosts().await()
        postById.putAll(posts.map { it.id to it })
        return posts
    }

    suspend fun getRelatedPosts(subcategoryUrl: String): List<Post> {
        val posts = api.getRelatedPosts(subcategoryUrl).await()
        postById.putAll(posts.map { it.id to it })
        return posts
    }

    fun getPost(postId: String): Post = postById[postId]!!

    suspend fun getComments(postId: String) = api.getComments(postId).await()
}