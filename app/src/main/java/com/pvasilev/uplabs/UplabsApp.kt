package com.pvasilev.uplabs

import android.app.Application
import com.jakewharton.threetenabp.AndroidThreeTen
import com.pvasilev.uplabs.di.AppModule
import com.pvasilev.uplabs.di.DI
import com.pvasilev.uplabs.di.NavigationModule
import toothpick.Toothpick

class UplabsApp : Application() {
    override fun onCreate() {
        super.onCreate()
        AndroidThreeTen.init(this)
        Toothpick.openScope(DI.APP_SCOPE).installModules(AppModule(), NavigationModule())
    }
}